package service;

import io.grpc.stub.StreamObserver;
import proto.ZodiacSignGrpc;
import proto.ZodiacSigns;

public class Implementation extends ZodiacSignGrpc.ZodiacSignImplBase {


    @Override
    public void return_(ZodiacSigns.DateRequest request, StreamObserver<ZodiacSigns.DateReply> responseObserver) {
        String string = request.getDate();
        int day = 0, month = 0, year = 0;
        boolean leapyear = false;
        //if ((string.matches("\\d{2}/\\d{2}/\\d{4}")) || (string.matches("\\d{1}/\\d{1}/\\d{4}"))) {
        if (((string.matches("\\d{2}/\\d{2}/\\d{4}")) || (string.matches("\\d{1}/\\d{1}/\\d{4}"))) && ((day <= 31) && (month <= 12))) {

            if (string.length() == 10) {
                day = Integer.parseInt(string.substring(0, 2));
                month = Integer.parseInt(string.substring(3, 5));
                year = Integer.parseInt(string.substring(6, 10));
            } else if (string.length() == 8) {
                day = Integer.parseInt(string.substring(0, 1));
                month = Integer.parseInt(string.substring(2, 3));
                year = Integer.parseInt(string.substring(4, 8));

            }
            if ((year % 100 == 0) && (year % 400 == 0) || (year / 4 == 0)) {
                leapyear = true;                      ////LEAPYEAR VERIFICATION
            }
            if (((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day > 30)) {
                System.out.println("Data introdusa este incorecta");
            } else if (((month == 2) && (leapyear == false) && (day > 28)) || ((month == 2)) && (leapyear == true) && (day > 29)) {
                System.out.println("incorect date");
            } else {
                switch (month) {
                    case 1:

                        if (day < 20) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " CAPRICORN").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " AQUARIUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                        break;
                    case 2:

                        if (day < 19) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " AQUARIUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " PISCES").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }
                        break;
                    case 3:

                        if (day < 21) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " PISCES").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " ARIES").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                        break;
                    case 4:

                        if (day < 20) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " ARIES").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " TAURUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                        break;
                    case 5:

                        if (day < 21) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " TAURUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " GEMINI").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                        break;
                    case 6:

                        if (day < 21) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " GEMINI").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " CANCER").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                    break;
                    case 7:

                        if (day < 23) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " CANCER").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " LEO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }
                    break;
                    case 8:

                        if (day < 23) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " LEO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " VIRGO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                    break;
                    case 9:
                    if (month == 9) {

                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " VIRGO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " LIBRA").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();

                    }
                    break;
                    case 10:

                        if (day < 23) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " LIBRA").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " SCORPIO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                    break;
                    case 11:

                        if (day < 22) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " SCORPIO").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " SAGITTARIUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                    break;
                    case 12:

                        if (day < 22) {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " SAGITTARIUS").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        } else {
                            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Your Zodiak Sign is" + " CAPRICORN").build();
                            responseObserver.onNext(reply1);
                            responseObserver.onCompleted();
                        }

                    break;

                }
            }


        } else {
            ZodiacSigns.DateReply reply1 = ZodiacSigns.DateReply.newBuilder().setMessage("Ati introdus date incorecte").build();
            responseObserver.onNext(reply1);
            responseObserver.onCompleted();
        }
    }

}





