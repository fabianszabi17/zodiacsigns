package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: ZodiacSigns.proto")
public final class ZodiacSignGrpc {

  private ZodiacSignGrpc() {}

  public static final String SERVICE_NAME = "ZodiacSign";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.ZodiacSigns.DateRequest,
      proto.ZodiacSigns.DateReply> getReturnMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Return",
      requestType = proto.ZodiacSigns.DateRequest.class,
      responseType = proto.ZodiacSigns.DateReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.ZodiacSigns.DateRequest,
      proto.ZodiacSigns.DateReply> getReturnMethod() {
    io.grpc.MethodDescriptor<proto.ZodiacSigns.DateRequest, proto.ZodiacSigns.DateReply> getReturnMethod;
    if ((getReturnMethod = ZodiacSignGrpc.getReturnMethod) == null) {
      synchronized (ZodiacSignGrpc.class) {
        if ((getReturnMethod = ZodiacSignGrpc.getReturnMethod) == null) {
          ZodiacSignGrpc.getReturnMethod = getReturnMethod = 
              io.grpc.MethodDescriptor.<proto.ZodiacSigns.DateRequest, proto.ZodiacSigns.DateReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "ZodiacSign", "Return"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.ZodiacSigns.DateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.ZodiacSigns.DateReply.getDefaultInstance()))
                  .setSchemaDescriptor(new ZodiacSignMethodDescriptorSupplier("Return"))
                  .build();
          }
        }
     }
     return getReturnMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ZodiacSignStub newStub(io.grpc.Channel channel) {
    return new ZodiacSignStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ZodiacSignBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ZodiacSignBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ZodiacSignFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ZodiacSignFutureStub(channel);
  }

  /**
   */
  public static abstract class ZodiacSignImplBase implements io.grpc.BindableService {

    /**
     */
    public void return_(proto.ZodiacSigns.DateRequest request,
        io.grpc.stub.StreamObserver<proto.ZodiacSigns.DateReply> responseObserver) {
      asyncUnimplementedUnaryCall(getReturnMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getReturnMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.ZodiacSigns.DateRequest,
                proto.ZodiacSigns.DateReply>(
                  this, METHODID_RETURN)))
          .build();
    }
  }

  /**
   */
  public static final class ZodiacSignStub extends io.grpc.stub.AbstractStub<ZodiacSignStub> {
    private ZodiacSignStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignStub(channel, callOptions);
    }

    /**
     */
    public void return_(proto.ZodiacSigns.DateRequest request,
        io.grpc.stub.StreamObserver<proto.ZodiacSigns.DateReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getReturnMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ZodiacSignBlockingStub extends io.grpc.stub.AbstractStub<ZodiacSignBlockingStub> {
    private ZodiacSignBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignBlockingStub(channel, callOptions);
    }

    /**
     */
    public proto.ZodiacSigns.DateReply return_(proto.ZodiacSigns.DateRequest request) {
      return blockingUnaryCall(
          getChannel(), getReturnMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ZodiacSignFutureStub extends io.grpc.stub.AbstractStub<ZodiacSignFutureStub> {
    private ZodiacSignFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ZodiacSignFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ZodiacSignFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ZodiacSignFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.ZodiacSigns.DateReply> return_(
        proto.ZodiacSigns.DateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getReturnMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_RETURN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ZodiacSignImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ZodiacSignImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_RETURN:
          serviceImpl.return_((proto.ZodiacSigns.DateRequest) request,
              (io.grpc.stub.StreamObserver<proto.ZodiacSigns.DateReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ZodiacSignBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ZodiacSignBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.ZodiacSigns.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ZodiacSign");
    }
  }

  private static final class ZodiacSignFileDescriptorSupplier
      extends ZodiacSignBaseDescriptorSupplier {
    ZodiacSignFileDescriptorSupplier() {}
  }

  private static final class ZodiacSignMethodDescriptorSupplier
      extends ZodiacSignBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ZodiacSignMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ZodiacSignGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ZodiacSignFileDescriptorSupplier())
              .addMethod(getReturnMethod())
              .build();
        }
      }
    }
    return result;
  }
}
