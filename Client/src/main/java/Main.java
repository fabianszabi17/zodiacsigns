import com.sun.org.apache.xpath.internal.operations.Bool;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.ZodiacSignGrpc;
import proto.ZodiacSigns;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        ZodiacSignGrpc.ZodiacSignStub Star = ZodiacSignGrpc.newStub(channel);

        System.out.println("1) Add a date");
        System.out.println("0) Exit");

        int option = -1;
        while (option != 0) {

            System.out.print("Introduceti o optiune: " + "Optiunile sunt 1 si 0");
            System.out.println();
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
            int month = 0, day = 0, year = 0;
            Boolean leapyear = false;


            switch (option) {
                case 1:
                    Scanner read = new Scanner(System.in);
                    System.out.print("Introduceti data dorita ");
                    String date = read.nextLine();

                    if (date.length() == 10) {
                        day = Integer.parseInt(date.substring(0, 2));
                        month = Integer.parseInt(date.substring(3, 5));
                        year = Integer.parseInt(date.substring(6, 10));

                    } else if (date.length() == 8) {
                        day = Integer.parseInt(date.substring(0, 1));
                        month = Integer.parseInt(date.substring(2, 3));
                        year = Integer.parseInt(date.substring(4, 8));

                    }
                    if ((year % 100 == 0) && (year % 400 == 0) || (year / 4 == 0)) {
                        leapyear = true;                      ////LEAPYEAR VERIFICATION
                    }

                    if (((date.matches("\\d{2}/\\d{2}/\\d{4}")) || (date.matches("\\d{1}/\\d{1}/\\d{4}"))) && ((day <= 31) && (month <= 12))) {
                        if (((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day > 30)) {
                            System.out.println("Data introdusa este incorecta");
                        } else {
                            if (((month == 2) && (leapyear == false) && (day > 28)) || ((month == 2)) && (leapyear == true) && (day > 29)) {
                                System.out.println("incorect date");
                            } else {
                                Star.return_(
                                        ZodiacSigns.DateRequest.newBuilder().setDate(date).build(),
                                        new StreamObserver<ZodiacSigns.DateReply>() {
                                            @Override
                                            public void onNext(ZodiacSigns.DateReply reply) {
                                                System.out.println(reply);
                                            }

                                            @Override
                                            public void onError(Throwable throwable) {
                                                System.out.println("Error: " + throwable.getMessage());
                                            }

                                            @Override
                                            public void onCompleted() {

                                            }
                                        }
                                );
                            }
                        }

                    } else
                        System.out.println("Data introdusa este incorecta");
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Comanda nu este disponibila");
                    break;
            }

        }

        channel.shutdown();
    }
}

